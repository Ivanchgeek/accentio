package main

import (
	"flag"
	"log"
	"net/http"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/db"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/handlers"

	"github.com/gorilla/mux"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/judge"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/onlinewatcher"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/sess"
)

var (
	configFile string
	stream     *onlinewatcher.Streamer
)

func init() {
	flag.StringVar(&configFile, "conf", "dev.json", "config file")
	flag.Parse()
	log.Println("setting up with " + configFile)
	config.Init(configFile)
	sess.Init()
	db.Init()
	onlineChan := make(chan string)
	stream = onlinewatcher.NewStreamer(onlineChan)
	go stream.Run()
	go judge.Run(config.Get().JudgeTimeout)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/oauth-enter", handlers.OAuthEnterHandler)
	r.HandleFunc("/oauth", handlers.OAuthHandler)
	r.HandleFunc("/deffault/add", handlers.AddToDeffaultHandler)
	r.HandleFunc("/deffault/remove", handlers.RemoveFromDeffaultHandler)
	r.HandleFunc("/user/me", handlers.GetMeHandler)
	r.HandleFunc("/user/logout", handlers.LogoutHandler)
	r.HandleFunc("/user/attach", func(w http.ResponseWriter, r *http.Request) {
		session, err := sess.Store().Get(r, config.Get().AuthSession)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			http.Error(w, "auth error", http.StatusUnauthorized)
			return
		}
		onlinewatcher.Attach(session.Values["vk"].(string), stream, w, r)
	})
	r.HandleFunc("/dictionary/register", handlers.RegisterAnswerHandler)

	log.Println("Listen and serve " + config.Get().Port)
	log.Println("#########")
	err := http.ListenAndServe(":"+config.Get().Port, r)
	if err != nil {
		panic(err)
	}
}
