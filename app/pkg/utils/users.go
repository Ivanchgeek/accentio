package utils

import (
	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
)

//IsAdmin - check if user - admin
func IsAdmin(vk string) bool {
	return StrInSlice(vk, config.Get().Admins)
}
