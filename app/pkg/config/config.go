package config

import (
	"encoding/json"
	"log"
	"os"
)

//Configuration - to parse json config
type Configuration struct {
	Port                   string
	DBName                 string
	UsersCollection        string
	DeffaultDictCollection string
	SessionKey             string
	AuthSession            string
	VkAppID                string
	OAuthRedirectURL       string
	VkAppSecret            string
	VkServiceToken         string
	Admins                 []string
	JudgeTimeout           int
}

var configuration *Configuration

//Init - initialize app configuration
func Init(name string) {
	file, err := os.Open("./src/gitlab.com/Ivanchgeek/accentio/app/" + name)
	if err != nil {
		panic(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configuration)
	if err != nil {
		panic(err)
	}
	log.Println("configuration parsed")
}

//Get - return link to current configuration
func Get() *Configuration {
	return configuration
}
