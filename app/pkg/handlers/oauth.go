package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/db"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/sess"
)

//OAuthEnterHandler - enter to oauth if not logged in
func OAuthEnterHandler(w http.ResponseWriter, r *http.Request) {
	session, _ := sess.Store().Get(r, config.Get().AuthSession)
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Redirect(w, r, "https://oauth.vk.com/authorize?"+
			"client_id="+config.Get().VkAppID+
			"&display=page&redirect_uri="+config.Get().OAuthRedirectURL+
			"&scope=&response_type=code&v=5.87", 302)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}

//OAuthHandler - vk oauth processor
func OAuthHandler(w http.ResponseWriter, r *http.Request) {
	//Auth user request
	resp, err := http.Get("https://oauth.vk.com/access_token?" +
		"client_id=" + config.Get().VkAppID +
		"&client_secret=" + config.Get().VkAppSecret +
		"&redirect_uri=" + config.Get().OAuthRedirectURL +
		"&code=" + r.FormValue("code"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var requestOAuth struct {
		ID    int    `json:"user_id"`
		Token string `json:"access_token"`
	}
	if err = json.NewDecoder(resp.Body).Decode(&requestOAuth); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//Get user's info
	resp, err = http.Get("https://api.vk.com/method/users.get?" +
		"user_ids=" + strconv.Itoa(requestOAuth.ID) +
		"&fields=&access_token=" + config.Get().VkServiceToken +
		"&v=5.87")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var requestInfo struct {
		Users []struct {
			ID int    `json:"id"`
			FN string `json:"first_name"`
			LN string `json:"last_name"`
		} `json:"response"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&requestInfo); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//register if new user
	exist, err := db.CheckUserExisted(strconv.Itoa(requestOAuth.ID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if !exist {
		err := db.CreateUser(strconv.Itoa(requestOAuth.ID), requestInfo.Users[0].FN+" "+requestInfo.Users[0].LN)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	//log in user
	session, _ := sess.Store().Get(r, config.Get().AuthSession)
	session.Values["authenticated"] = true
	session.Values["vk"] = strconv.Itoa(requestOAuth.ID)
	err = session.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, r.FormValue("state"), 302)
}
