package handlers

import (
	"net/http"
	"strconv"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/db"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/sess"
	"gopkg.in/mgo.v2/bson"
)

//RegisterAnswerHandler updates word's percentage and count in user's dictionary
func RegisterAnswerHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sess.Store().Get(r, config.Get().AuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusUnauthorized)
		return
	}
	if err := r.ParseForm(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	if !bson.IsObjectIdHex(r.FormValue("word")) {
		http.Error(w, "wrond word id", http.StatusBadRequest)
		return
	}
	step, err := strconv.ParseBool(r.FormValue("step"))
	if err != nil {
		http.Error(w, "wrond step type", http.StatusBadRequest)
		return
	}
	err = db.UpdateWordStats(session.Values["vk"].(string), bson.ObjectIdHex(r.FormValue("word")), step)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
}
