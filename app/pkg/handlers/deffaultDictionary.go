package handlers

import (
	"net/http"
	"strconv"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/db"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/sess"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/utils"
)

//AddToDeffaultHandler - add word to deffault dictionary handler
func AddToDeffaultHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sess.Store().Get(r, config.Get().AuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusUnauthorized)
		return
	}
	err = r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if utils.IsAdmin(session.Values["vk"].(string)) {
		accent, err := strconv.Atoi(r.FormValue("accent"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = db.AddToDeffault(r.FormValue("word"), int(accent))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("ok"))
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
}

//RemoveFromDeffaultHandler - remove word from deffault dictionary by content handler
func RemoveFromDeffaultHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sess.Store().Get(r, config.Get().AuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusUnauthorized)
		return
	}
	err = r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if utils.IsAdmin(session.Values["vk"].(string)) {
		err = db.RemoveFromDeffault(r.FormValue("word"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write([]byte("ok"))
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
}
