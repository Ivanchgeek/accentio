package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/db"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/sess"
)

//GetMeHandler - send info about logged n user
func GetMeHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sess.Store().Get(r, config.Get().AuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusUnauthorized)
		return
	}
	response, err := db.GetUser(session.Values["vk"].(string))
	if err != nil {
		if err.Error() == "not found" {
			http.Error(w, "auth error", http.StatusUnauthorized)
			return
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	js, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

//LogoutHandler - logout user
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sess.Store().Get(r, config.Get().AuthSession)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "auth error", http.StatusUnauthorized)
		return
	}
	session.Options.MaxAge = -1
	err = session.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("logouted"))
}
