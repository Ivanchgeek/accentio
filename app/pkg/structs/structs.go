package structs

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//Word - element of dictionary
type Word struct {
	ID         bson.ObjectId `json:"id" bson:"_id"`
	Percentage int
	Count      int
	Content    string
	Accent     int
}

//User - user info
type User struct {
	ID         bson.ObjectId `json:"id" bson:"_id"`
	VK         string
	Name       string
	Dictionary []Word
	Percentage int
	Rating     int
	TimeIn     time.Duration
	Notify     bool
	AutoUpdate bool
}

//DefDict - deffault dictionary (current)
type DefDict struct {
	Dictionary []Word
}
