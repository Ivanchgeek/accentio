package sess

import (
	"log"

	"github.com/gorilla/sessions"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
)

var store *sessions.CookieStore

//Init - init gorilla sessions
func Init() {
	store = sessions.NewCookieStore([]byte(config.Get().SessionKey))
	log.Println("session inited")
}

//Store - share session store
func Store() *sessions.CookieStore {
	return store
}
