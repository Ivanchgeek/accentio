package db

import (
	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/structs"
	"gopkg.in/mgo.v2/bson"
)

//GetAllUsers - get all users to collect stats
func GetAllUsers() ([]structs.User, error) {
	sess := session.Clone()
	defer sess.Close()
	var users []structs.User
	err := sess.DB(config.Get().DBName).C(config.Get().UsersCollection).Find(bson.M{}).All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}
