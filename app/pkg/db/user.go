package db

import (
	"log"
	"time"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/structs"
	"gopkg.in/mgo.v2/bson"
)

//CheckUserExisted - check if user with this vk in db
func CheckUserExisted(vk string) (bool, error) {
	sess := session.Clone()
	defer sess.Close()
	length, err := sess.DB(config.Get().DBName).C(config.Get().UsersCollection).Find(bson.M{"vk": vk}).Count()
	if err != nil {
		return false, err
	}
	return length != 0, nil
}

//CreateUser - create new user
func CreateUser(vk, name string) error {
	sess := session.Clone()
	defer sess.Close()
	dict, err := GetDeffault()
	if err != nil {
		return err
	}
	err = sess.DB(config.Get().DBName).C(config.Get().UsersCollection).Insert(structs.User{
		Name:       name,
		ID:         bson.NewObjectId(),
		VK:         vk,
		Dictionary: dict.Dictionary,
		TimeIn:     time.Microsecond * 0,
		Rating:     0})
	if err == nil {
		log.Println("new user created id: " + vk)
	}
	return err
}

//GetUser - get info about user
func GetUser(vk string) (structs.User, error) {
	sess := session.Clone()
	defer sess.Close()
	var user structs.User
	err := sess.DB(config.Get().DBName).C(config.Get().UsersCollection).Find(bson.M{"vk": vk}).One(&user)
	if err != nil {
		return structs.User{}, err
	}
	return user, nil
}

//AddTimeIn update user;'s online time summ
func AddTimeIn(vk string, add time.Duration) error {
	user, err := GetUser(vk)
	if err != nil {
		return err
	}
	sess := session.Clone()
	defer sess.Close()
	err = sess.DB(config.Get().DBName).C(config.Get().UsersCollection).Update(bson.M{"vk": vk}, bson.M{"$set": bson.M{"timein": user.TimeIn + add}})
	if err != nil {
		return err
	}
	return nil
}

//SetUserPercentage - u know:)
func SetUserPercentage(id bson.ObjectId, update int) error {
	sess := session.Clone()
	defer sess.Close()
	err := sess.DB(config.Get().DBName).C(config.Get().UsersCollection).UpdateId(id, bson.M{"$set": bson.M{"percentage": update}})
	if err != nil {
		return err
	}
	return nil
}

//SetUserRating - u know:)
func SetUserRating(id bson.ObjectId, update int) error {
	sess := session.Clone()
	defer sess.Close()
	err := sess.DB(config.Get().DBName).C(config.Get().UsersCollection).UpdateId(id, bson.M{"$set": bson.M{"rating": update}})
	if err != nil {
		return err
	}
	return nil
}
