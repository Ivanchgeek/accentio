package db

import (
	"log"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gopkg.in/mgo.v2"
)

var session *mgo.Session

//Init - initilize db
func Init() {
	//open session
	var err error
	session, err = mgo.Dial("mongodb:27017")
	if err != nil {
		panic(err)
	}
	log.Println("db inited: " + config.Get().DBName)
}
