package db

import (
	"errors"
	"log"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gitlab.com/Ivanchgeek/accentio/app/pkg/structs"
	"gopkg.in/mgo.v2/bson"
)

//AddToDeffault - update deffault dictionary
func AddToDeffault(word string, accent int) error {
	update := structs.Word{
		ID:         bson.NewObjectId(),
		Content:    word,
		Accent:     accent,
		Percentage: 0,
		Count:      0}
	sess := session.Clone()
	defer sess.Close()
	//check if existed
	length, err := sess.DB(config.Get().DBName).C(config.Get().DeffaultDictCollection).Find(bson.M{"dictionary.content": word}).Count()
	if err != nil {
		return err
	}
	if length == 0 {
		//add new word
		_, err = sess.DB(config.Get().DBName).C(config.Get().DeffaultDictCollection).Upsert(bson.M{}, bson.M{"$push": bson.M{"dictionary": update}})
		if err != nil {
			return err
		}
		log.Println("adding to deffault: " + word)
	} else {
		return errors.New("word " + word + " already existed in default dictionary")
	}
	return nil
}

//RemoveFromDeffault - remove word from deffault dictionary by content
func RemoveFromDeffault(word string) error {
	sess := session.Clone()
	defer sess.Close()
	err := sess.DB(config.Get().DBName).C(config.Get().DeffaultDictCollection).Update(bson.M{},
		bson.M{"$pull": bson.M{"dictionary": bson.M{"content": word}}})
	if err != nil {
		return err
	}
	log.Println("remove from deffault: " + word)
	return nil
}

//GetDeffault - get deffault dictionary
func GetDeffault() (structs.DefDict, error) {
	var result structs.DefDict
	sess := session.Clone()
	defer sess.Close()
	err := sess.DB(config.Get().DBName).C(config.Get().DeffaultDictCollection).Find(bson.M{}).One(&result)
	if err != nil {
		return structs.DefDict{}, err
	}
	return result, nil
}
