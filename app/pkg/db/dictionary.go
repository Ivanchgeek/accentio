package db

import (
	"errors"
	"log"
	"math"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/config"
	"gopkg.in/mgo.v2/bson"
)

//UpdateWordStats change word's percentage and count according to user's answer (step)
func UpdateWordStats(vk string, id bson.ObjectId, step bool) error {
	user, err := GetUser(vk)
	if err != nil {
		return err
	}
	var count, percentage int
	for i, word := range user.Dictionary {
		if id == word.ID {
			count = word.Count
			percentage = word.Percentage
			break
		}
		if i == len(user.Dictionary)-1 {
			return errors.New("no such word")
		}
	}
	right := count * percentage / 100
	wrong := count - right
	if step {
		right++
	} else {
		wrong++
	}
	count = right + wrong
	percentage = int(math.Round(float64(right) / float64(count) * 100))
	sess := session.Clone()
	defer sess.Close()
	err = sess.DB(config.Get().DBName).C(config.Get().UsersCollection).Update(
		bson.M{"vk": vk, "dictionary._id": id},
		bson.M{"$set": bson.M{"dictionary.$.percentage": percentage, "dictionary.$.count": count}})
	if err != nil {
		return err
	}
	log.Printf("user %v achived %v percentage in word %v with count %v", vk, percentage, id, count)
	return nil
}
