package onlinewatcher

import (
	"time"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/db"
)

//Streamer - streams logs to web
type Streamer struct {
	connections map[*Client]bool
	attach      chan *Client
	deattach    chan *Client
	input       chan string
}

//NewStreamer - create new streamer
func NewStreamer(input chan string) *Streamer {
	return &Streamer{
		connections: make(map[*Client]bool),
		attach:      make(chan *Client),
		deattach:    make(chan *Client),
		input:       input,
	}
}

//Run streamer
func (s *Streamer) Run() {
	for {
		select {
		case client := <-s.attach:
			s.connections[client] = true
		case client := <-s.deattach:
			if time.Now().After(client.connectTimeStamp) {
				db.AddTimeIn(client.vk, time.Now().Sub(client.connectTimeStamp))
			}
			if _, ok := s.connections[client]; ok {
				delete(s.connections, client)
				close(client.send)
			}
		case message := <-s.input:
			for client := range s.connections {
				select {
				case client.send <- []byte(message):
				default:
					close(client.send)
					delete(s.connections, client)
				}
			}
		}
	}
}
