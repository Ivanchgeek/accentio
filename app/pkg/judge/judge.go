package judge

import (
	"math"
	"sort"
	"time"

	"gitlab.com/Ivanchgeek/accentio/app/pkg/db"
)

//Run - start judge
func Run(timeout int) {
	for {
		time.Sleep(time.Duration(timeout) * time.Second)
		users, err := db.GetAllUsers()
		if err != nil {
			continue
		}
		for _, user := range users {
			if len(user.Dictionary) == 0 {
				continue
			}
			var average, sum int
			for _, word := range user.Dictionary {
				average += word.Percentage
				sum += word.Count
			}
			average = int(math.Round(float64(average / len(user.Dictionary))))
			if sum/len(user.Dictionary) < 10 {
				average = 0
			}
			err := db.SetUserPercentage(user.ID, average)
			if err != nil {
				continue
			}
		}
		users, err = db.GetAllUsers()
		if err != nil {
			continue
		}
		sort.Slice(users, func(i, j int) bool {
			return users[i].Percentage > users[j].Percentage
		})
		for place, user := range users {
			if user.Percentage == 0 {
				db.SetUserRating(user.ID, 0)
			} else {
				db.SetUserRating(user.ID, int(math.Round(float64((len(users)-place)/len(users))*100)))
			}
		}
	}
}
