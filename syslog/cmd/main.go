package main

import (
	"flag"
	"log"
	"net/http"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/handlers"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/structs"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/config"
	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/streamer"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/db"
	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/logger"
)

var (
	configFile string
	stream     *streamer.Streamer
)

func init() {
	flag.StringVar(&configFile, "conf", "dev.json", "config file")
	flag.Parse()
	config.Init(configFile)
	db.Init()
	logChan := make(chan structs.Log)
	stream = streamer.NewStreamer(logChan)
	go stream.Run()
	go logger.Init(logChan)
}

func echo(w http.ResponseWriter, r *http.Request) {
	streamer.Attach(stream, w, r)
}

func main() {
	http.HandleFunc("/stream", func(w http.ResponseWriter, r *http.Request) {
		streamer.Attach(stream, w, r)
	})
	http.HandleFunc("/get/apps", handlers.GetAppsHandler)
	http.HandleFunc("/get/logs", handlers.GetLogsHandler)
	log.Fatal(http.ListenAndServe(":8081", nil))
}
