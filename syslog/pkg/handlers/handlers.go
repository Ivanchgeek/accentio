package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/db"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/config"
)

//GetAppsHandler - get all aviable apps
func GetAppsHandler(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Key string `json:"key"`
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if request.Key == config.Get().Key {
		js, err := json.Marshal(config.Get().Subscribers)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
}

//GetLogsHandler - get logs by app name
func GetLogsHandler(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Key string `json:"key"`
		App string `json:"app"`
	}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if request.Key == config.Get().Key {
		logs, err := db.GetLogs(request.App)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		js, err := json.Marshal(logs)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	} else {
		http.Error(w, "permission denied", http.StatusMethodNotAllowed)
		return
	}
}
