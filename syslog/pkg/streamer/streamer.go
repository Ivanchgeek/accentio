package streamer

import (
	"encoding/json"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/structs"
)

//Streamer - streams logs to web
type Streamer struct {
	connections map[*Client]bool
	attach      chan *Client
	deattach    chan *Client
	input       chan structs.Log
}

//NewStreamer - create new streamer
func NewStreamer(input chan structs.Log) *Streamer {
	return &Streamer{
		connections: make(map[*Client]bool),
		attach:      make(chan *Client),
		deattach:    make(chan *Client),
		input:       input,
	}
}

//Run streamer
func (s *Streamer) Run() {
	for {
		select {
		case client := <-s.attach:
			s.connections[client] = true
		case client := <-s.deattach:
			if _, ok := s.connections[client]; ok {
				delete(s.connections, client)
				close(client.send)
			}
		case message := <-s.input:
			messageJSON, _ := json.Marshal(message)
			for client := range s.connections {
				select {
				case client.send <- messageJSON:
				default:
					close(client.send)
					delete(s.connections, client)
				}
			}
		}
	}
}
