package config

import (
	"encoding/json"
	"log"
	"os"
)

//Configuration - to parse json config
type Configuration struct {
	Port             string
	DBName           string
	CollectiosPrefix string
	Separator        string
	Subscribers      []string
	Viewers          []string
	Key              string
}

var configuration *Configuration

//Init - initialize app configuration
func Init(name string) {
	file, err := os.Open("./src/gitlab.com/Ivanchgeek/accentio/syslog/" + name)
	if err != nil {
		panic(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configuration)
	if err != nil {
		panic(err)
	}
	log.Println("configuration parsed")
}

//Get - return link to current configuration
func Get() *Configuration {
	return configuration
}
