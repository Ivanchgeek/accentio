package db

import (
	"log"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/structs"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/config"
	"gopkg.in/mgo.v2"
)

var session *mgo.Session

//Init - initilize db
func Init() {
	//open session
	var err error
	session, err = mgo.Dial("mongodb:27017")
	if err != nil {
		panic(err)
	}
	log.Println("db inited: " + config.Get().DBName)
}

//WriteLog - write one log string
func WriteLog(newLog structs.Log) error {
	sess := session.Clone()
	return sess.DB(config.Get().DBName).C(config.Get().CollectiosPrefix + newLog.App).Insert(newLog)
}

//GetLogs - get all app's logs
func GetLogs(app string) ([]structs.Log, error) {
	sess := session.Clone()
	var result []structs.Log
	err := sess.DB(config.Get().DBName).C(config.Get().CollectiosPrefix + app).Find(bson.M{}).All(&result)
	if err != nil {
		return []structs.Log{}, err
	}
	return result, nil
}
