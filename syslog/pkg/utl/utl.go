package utl

import (
	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/config"
)

//IsSubscriber - check if app subscribed to syslog
func IsSubscriber(app string) bool {
	for _, sub := range config.Get().Subscribers {
		if app == sub {
			return true
		}
	}
	return false
}

//IsViewer - check if user have a permission to view logs
func IsViewer(vk string) bool {
	for _, viewer := range config.Get().Viewers {
		if vk == viewer {
			return true
		}
	}
	return false
}
