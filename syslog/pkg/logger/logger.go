package logger

import (
	"log"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/utl"

	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/db"
	"gitlab.com/Ivanchgeek/accentio/syslog/pkg/structs"

	"gopkg.in/mcuadros/go-syslog.v2"
)

//Init - init listening on syslog server
func Init(output chan structs.Log) {
	log.Println("Initilizing syslog")
	channel := make(syslog.LogPartsChannel)
	handler := syslog.NewChannelHandler(channel)

	server := syslog.NewServer()
	server.SetFormat(syslog.RFC5424)
	server.SetHandler(handler)
	server.ListenUDP(":514")
	server.Boot()

	go func(channel syslog.LogPartsChannel, output chan structs.Log) {
		for logParts := range channel {
			app, appOk := logParts["app_name"].(string)
			msg, msgOk := logParts["message"].(string)
			if msgOk && appOk && utl.IsSubscriber(app) {
				newLog := structs.Log{Info: msg, App: app}
				output <- newLog
				err := db.WriteLog(newLog)
				if err != nil {
					log.Println(err)
				}
			}
		}
	}(channel, output)

	log.Println("Syslog server inited")
	server.Wait()
}
