import React from 'react';
import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      apps: [],
      logs: [],
      ascroll: false,
      app: "",
      new: new Map()
    }
  }

  ws = new WebSocket('ws://localhost:8081/stream')

  componentDidMount = async () => {
    window.sessionStorage.setItem("key",prompt("enter access key"), "key")
    this.setState({ascroll: confirm("enable autoscroll?")})
    await fetch("/get/apps", {
      method: "post",
      body: JSON.stringify({
        key: window.sessionStorage.getItem("key")
      })
    }).then(resp => {
      if (resp.status == 405) {
        alert("Wrong key")
        window.location.reload()
        return
      }
      if (resp.status != 200) {
        alert("server error")
        return
      }
      return resp.json()
    }).then(js => {
      this.setState({apps: js})
      var newUdp = new Map()
      for (var i = 0; i < this.state.apps.length; i++) {
        newUdp.set(this.state.apps[i], 0)
      }
      this.setState({new: newUdp})
    }).catch(e => {
      console.log(e)
    })

    this.ws.onmessage = evt => {
      const message = JSON.parse(evt.data)
      if (this.state.app == message.App) {
        var add = this.state.logs
        add.push(message)
        this.setState({logs: add})
        if (this.state.ascroll) document.getElementById("bottom").click()
      } else {
        var newUdp = this.state.new
        newUdp.set(message.App, newUdp.get(message.App) + 1)
        this.setState({new: newUdp})
      }
    }

    this.ws.onclose = () => {
      alert("stream disconected")
    }
  }

  navClick = async event => {
    this.setState({app: event.target.dataset.app})
    var newUdp = this.state.new
    newUdp.set(event.target.dataset.app, 0)
    this.setState({new: newUdp})
    await fetch("/get/logs", {
      method: "post",
      body: JSON.stringify({
        key: window.sessionStorage.getItem("key"),
        app: event.target.dataset.app
      })
    }).then(resp => {
      if (resp.status == 405) {
        alert("Wrong key")
        window.location.reload()
        return
      }
      if (resp.status != 200) {
        alert("server error")
        return
      }
      return resp.json()
    }).then(js => {
      if (js != null) this.setState({logs: js}); else
      this.setState({logs: [{Info: "no logs yet"}]})
    }).catch(e => {
      console.log(e)
    })
    document.getElementById("bottom").click()
  }

  render() {
    return (
      <div>
        <div className="nav">
          {this.state.apps.map(app => {
              return (
              <button className={"button " + (this.state.app == app ? "active" : "")}key={app} onClick={this.navClick} data-app={app}>{app}<span style={{display: (this.state.new.get(app) == 0 ? "none" : "")}}>{this.state.new.get(app)} new lines</span></button>
              )
          })}
        </div>
        <div className="terminal">
          {this.state.logs.map((line, i) => {
              return (
                <h1 key={i}>{line.Info}</h1>
              )
          })}
          <a href="#bottom" id="bottom"></a>
        </div>
      </div>
    );
  }
}

export default App;
