const initialState = {
    processing: false,
    activeView: "",
    ponger: WebSocket
}

export default function share(state = initialState, action) {
    switch (action.type) {
        case "SetProcessing":
            return ({
                ...state,
                processing: action.value
            })
        case "SetActiveView":
                return ({
                    ...state,
                    activeView: action.value
                })
        case "SetPonger":
            return ({
                ...state,
                ponger: action.value
            })
        default:
            return state
    }
}