import { combineReducers } from 'redux'

import nav from "./nav"
import user from "./user"
import login from "./login"
import share from "./share"
import game from "./game"

export default combineReducers({
    nav,
    user,
    login,
    share,
    game
})
