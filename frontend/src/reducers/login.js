const initialState = {
    hiding: false
}

export default function login(state = initialState, action) {
    switch (action.type) {
        case "SetLoginHiding":
                return ({
                    ...state,
                    hiding: action.value
                })
        default:
            return state
    }
}