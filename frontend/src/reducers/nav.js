const initialState = {
    opened: false
}

export default function nav(state = initialState, action) {
    switch (action.type) {
        case "SetNavOpened":
            return ({
                ...state,
                opened: action.value
            })
        default:
            return state
    }
}