const initialState = {
    word: {Content: ""},
    active: -2,
    status: 0,
    impuls: 0,
    wordDistribution: []
}

export default function lgame(state = initialState, action) {
    switch (action.type) {
        case "SetGameActive":
                return ({
                    ...state,
                    active: action.value
                })
        case "SetGameWord":
            return ({
                ...state,
                word: action.value
            })
        case "SetGameStatus":
            return ({
                ...state,
                status: action.value
            })
        case "SetGameImpuls":
            return ({
                ...state,
                impuls: action.value
            })
        case "SetWordDistribution":
            return ({
                ...state,
                wordDistribution: action.value
            })
        default:
            return state
    }
}