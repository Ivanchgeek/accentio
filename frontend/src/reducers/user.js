const initialState = {
    loggedIn: false,
    user: {}
}

export default function user(state = initialState, action) {
    switch (action.type) {
        case "SetUserLoggedIn":
            return ({
                ...state,
                loggedIn: action.value
            })
        case "SetUser":
            return ({
                ...state,
                user: action.value
            })
        case "IncTimeIn": 
            return ({
                ...state,
                user: {
                    ...state.user,
                    TimeIn: state.user.TimeIn+1
                }
            })
        default:
            return state
    }
}