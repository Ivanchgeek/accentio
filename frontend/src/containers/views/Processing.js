import React from 'react';
import '../../styles/Processing.sass';
import logo from "../../stuff/icon_logo.svg"

class Processing extends React.Component {

    render() {
        return (
            <div className="view-wrap">
                <div className="view processing">
                    <div className="bar"><div className="racer"></div></div>
                </div>
            </div>
        );
    }
}

export default (Processing);