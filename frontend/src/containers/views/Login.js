import React from 'react';
import '../../styles/Login.sass';
import {connect} from "react-redux"
import Mountains from "../jsx/mountains"

class Login extends React.Component {

    login = () => {
        window.location.replace("https://oauth.vk.com/authorize?"+
        "client_id=7219480"+
        "&display=page&redirect_uri=http://" + window.location.hostname + ":8080/oauth"+
        "&scope=&response_type=code&v=5.87&state="+window.location)
    }

    render() {
        return (
            <div className="view-wrap">
                <div className="view login">
                    <div className="textWrap">
                        <h1>Добро пожаловать в</h1>
                        <h1>Accentio</h1>
                        <button onClick={this.login}>войти</button>
                    </div>
                    <Mountains></Mountains>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    hiding: state.user.hiding
});
export default connect(mapStateToProps)(Login);