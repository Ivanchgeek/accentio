import React from 'react';
import '../../styles/Profile.sass';
import {connect} from "react-redux"
import {UpdateUser} from "../../actions/user"


class Profile extends React.Component {

    componentDidMount = () => {
        this.props.dispatch({type: "SetActiveView", value: "profile"})
        this.props.dispatch(UpdateUser(this.props.user.TimeIn))
    }

    render() {
        const {user} = this.props
        var date = new Date(null)
        date.setSeconds(this.props.user.TimeIn)
        var formatDate = date.toISOString().substr(11, 8)
        return (
            <div className="view profile">
                <h1 className="first">Привет,</h1>
                <h1>{this.props.user.Name}!</h1>
                <h2 className="first">Вы подготовлены на <span style={
                    {color: (user.Percentage > 30 ? (user.Percentage > 70 ? "#78CA5B" : "#DBDE29") : "#EF4A4A")}
                }>{user.Percentage}</span>%</h2>
                <h2 className="first">Это лучше, чем <span style={
                    {color: (user.Rating > 30 ? (user.Rating > 70 ? "#78CA5B" : "#DBDE29") : "#EF4A4A")}
                }>{user.Rating}</span>% пользователей</h2>
                <h2>Вы провели {formatDate} за тренировками</h2>
                <h2>Не сдавайтесь, и все получится:)</h2>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.user.user
});
export default connect(mapStateToProps)(Profile);