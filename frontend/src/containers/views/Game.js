import React from 'react';
import '../../styles/Game.sass';
import {connect} from "react-redux"
import {CSSTransition} from "react-transition-group"
import {RegisterWordStep, InitWordDistribution} from "../../actions/dictionary"

class Game extends React.Component {

    newWord = () => {
        const {dict, distribution} = this.props
        if (distribution.length > 0) {
            this.props.dispatch({type: "SetGameWord", value: distribution[Math.floor((Math.random() * distribution.length))]})
        } else {
            this.props.dispatch({type: "SetGameWord", value: dict[Math.floor((Math.random() * dict.length))]})
        }
    }

    submitLetter = () => {
        if (this.props.active != -2) {
            this.props.dispatch({type: "SetGameStatus", value: this.props.active == this.props.word.Accent ? 1 : 2})
            this.props.dispatch(RegisterWordStep(this.props.word.id, this.props.active == this.props.word.Accent))
            var impulser = setInterval(() => {
                this.props.dispatch({type: "SetGameImpuls", value: this.props.impuls+1})
            }, 50)
            var len = this.props.word.Content.length
            setTimeout(() => {
                clearInterval(impulser)
                this.props.dispatch({type: "SetGameActive", value: -2})
                this.props.dispatch({type: "SetGameStatus", value: 3})
                this.props.dispatch({type: "SetGameImpuls", value: 0})
                setTimeout(() => {
                    this.props.dispatch({type: "SetGameStatus", value: 0})
                }, 500)
                setTimeout(() => {
                    this.newWord()
                }, 250)
            }, 50 * ((this.props.active > len) ? this.props.active : len) - 40)
        }
    }

    componentWillMount = () => {
        this.props.dispatch(InitWordDistribution(this.props.dict))
        this.newWord()
    }

    componentDidMount = () => {
        this.props.dispatch({type: "SetActiveView", value: "game"})
    }

    letterTouch = event => {
        var letter = document.elementFromPoint(event.touches[0].pageX, event.touches[0].pageY)
        this.props.dispatch({type: "SetGameActive", value: parseInt(letter.dataset.me)})
    }

    render() {
        const {active, word, impuls, status} = this.props
        return (
            <div className="view game">
                <CSSTransition
                        in = {this.props.status != 3}
                        timeout={{ enter: 200, exit: 200 }}
                        classNames="fade">
                    <div className="word" onTouchMove={this.letterTouch} data-me="-2" onTouchEnd={this.submitLetter}>
                        {word.Content.split("").map((l, i) => {
                            return (
                                <div className="letter" key={i} data-me={i}>
                                    <p data-me={i} 
                                        className={(i == active && status == 0? "active " : "") + 
                                        ((i == active - 1 || i == active + 1) && status == 0 ? "subActive " : "") + 
                                        (active <= i && active + impuls >= i && status != 0 ? (status == 1 ? "correct " : "incorrect ") : "") +
                                        (active > i && active - impuls <= i && status != 0 ? (status == 1 ? "correct " : "incorrect ") : "")}
                                    >{l}</p>
                                </div>
                            )
                        })}
                    </div>
                </CSSTransition>
                <h1>проведите, что бы выбрать букву</h1>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    word: state.game.word,
    active: state.game.active,
    dict: state.user.user.Dictionary,
    status: state.game.status,
    impuls: state.game.impuls,
    distribution: state.game.wordDistribution
});
export default connect(mapStateToProps)(Game);