import React from "react"

const Mountains = () => {
    return (
        <svg width="100%" viewBox="0 0 360 363" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
                <g filter="url(#filter0_d)">
                    <path d="M154.1 37L220.784 224.5H87.4161L154.1 37Z" fill="#78CA5B" id="m3body" />
                </g>
                <path d="M140.1 93.5L131.1 101L154.1 37L175.6 98L171.1 93.5L163.6 100L156.1 93.5L147.6 100L140.1 93.5Z" fill="white" id="m3top" />
                <g filter="url(#filter1_d)">
                    <path d="M35.1 188L101.784 375.5H-31.5839L35.1 188Z" fill="#78CA5B" id="m1body" />
                </g>
                <g filter="url(#filter2_d)">
                    <path d="M230.6 24L355.741 375H105.459L230.6 24Z" fill="#78CA5B" id="m4body" />
                </g>
                <path d="M217.1 80.5L208.1 88L231.1 24L252.6 85L248.1 80.5L240.6 87L233.1 80.5L224.6 87L217.1 80.5Z" fill="white" id="m4top" />
                <path d="M230.6 24L172.1 375H105.459L230.6 24Z" fill="black" fill-opacity="0.29" id="m4shad" />
                <path d="M153.941 37L95.4407 388H28.8L153.941 37Z" fill="black" fill-opacity="0.29" id="m3shad" />
                <g filter="url(#filter3_d)">
                    <path d="M112.1 101L209.095 373.25H15.1052L112.1 101Z" fill="#78CA5B" id="m2body" />
                </g>
                <g filter="url(#filter4_d)">
                    <path d="M306.1 101L403.095 373.25H209.105L306.1 101Z" fill="#78CA5B" id="m5body" />
                </g>
                <path d="M98.1 157.5L89.1 165L112.1 101L133.6 162L129.1 157.5L121.6 164L114.1 157.5L105.6 164L98.1 157.5Z" fill="white" id="m2top" />
                <path d="M292.1 157.5L283.1 165L306.1 101L327.6 162L323.1 157.5L315.6 164L308.1 157.5L299.6 164L292.1 157.5Z" fill="white" id="m5top" />
                <path d="M305.941 101L247.441 452H180.8L305.941 101Z" fill="black" fill-opacity="0.29" id="m5shad" />
                <path d="M111.941 101L53.4407 452H-13.2L111.941 101Z" fill="black" fill-opacity="0.29" id="m2shad" />
                <path d="M35.1407 188L-23.3593 539H-90L35.1407 188Z" fill="black" fill-opacity="0.29" id="m1shad" />
            </g>
            <defs>
                <filter id="filter0_d" x="83.416" y="33" width="141.368" height="195.5" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.78 0" />
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                </filter>
                <filter id="filter1_d" x="-35.584" y="184" width="141.368" height="195.5" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.78 0" />
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                </filter>
                <filter id="filter2_d" x="101.459" y="20" width="258.281" height="359" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.78 0" />
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                </filter>
                <filter id="filter3_d" x="11.1052" y="97" width="201.99" height="280.25" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.78 0" />
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                </filter>
                <filter id="filter4_d" x="205.105" y="97" width="201.99" height="280.25" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.78 0" />
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                </filter>
                <clipPath id="clip0">
                    <rect width="360" height="363" fill="white" />
                </clipPath>
            </defs>
        </svg>
    )
}

export default Mountains

