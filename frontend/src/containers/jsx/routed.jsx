import React from "react"
import {Route, Switch } from 'react-router-dom'
import {CSSTransition, TransitionGroup} from "react-transition-group"

import Profile from "../views/Profile"
import Game from "../views/Game"

const Routed = () => {
    return (
        <Route render={({location}) => {
            return (
                <TransitionGroup className="view-wrap">
                    <CSSTransition
                        key={location.key}
                        timeout={{ enter: 500, exit: 500 }}
                        classNames="view">
                        <Switch location={location}>
                            <Route exact path="/" component={Game}/>
                            <Route path="/profile" component={Profile}/>
                        </Switch>
                    </CSSTransition>
                </TransitionGroup>
            )
        }}/>
    )
}

export default Routed