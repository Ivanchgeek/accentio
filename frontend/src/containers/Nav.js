import React from 'react';
import '../styles/Nav.sass';
import {connect} from "react-redux"
import logo from "../stuff/logo.svg"
import { NavLink } from 'react-router-dom'
import {CSSTransition} from "react-transition-group"
import {LogoutUser} from "../actions/user"

class Nav extends React.Component {

    switchNav = event => {
        this.props.dispatch({type: "SetNavOpened", value: !this.props.opened})
    }

    logout = event => {
        this.switchNav(); 
        this.props.ponger.close()
        this.props.dispatch({type: "SetPonger", value: null})
        this.props.dispatch(LogoutUser());
    }

    render() {
        const {activeView} = this.props
        return (
            <div className="nav">
                <div className="left">
                    <img src={logo}></img>
                    <h1>Accentio</h1>
                </div>
                <CSSTransition
                    in = {this.props.loggedIn && !this.props.processing}
                    timeout={{ enter: 500, exit: 0 }}
                    classNames="barSlide"
                    unmountOnExit>
                        <div className={"bar " + (this.props.opened ? "cross" : "")} onClick={this.switchNav}>
                            <div className="stick"></div>
                            <div className="stick"></div>
                            <div className="stick"></div>
                        </div>
                </CSSTransition>
                <div className={"linkHouse " + (this.props.opened ? "opened" : "")}>
                    <NavLink to="/" className="link" onClick={this.switchNav} style={
                        {opacity: (activeView == "game" ? ".5" : "1"), pointerEvents: (activeView == "game" ? "none" : "")}
                    }>Игра</NavLink>
                    <NavLink to="/profile" className="link" onClick={this.switchNav} style={
                        {opacity: (activeView == "profile" ? ".5" : "1"), pointerEvents: (activeView == "profile" ? "none" : "")}
                    }>Профиль</NavLink>
                    <NavLink to="/" className="link" onClick={this.logout}>Выйти</NavLink>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    opened: state.nav.opened,
    loggedIn: state.user.loggedIn,
    processing: state.share.processing,
    activeView: state.share.activeView,
    ponger: state.share.ponger
});
export default connect(mapStateToProps)(Nav);