import React from 'react';
import '../styles/Desktop.sass';
import logo from "../stuff/logo.svg"

class Desktop extends React.Component {

    render() {
        return (
            <div className="desktopCurtain">
                <img src={logo}></img>
                <h1>Возвращайтесь к нам с телефона :)</h1>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({

});
export default Desktop;
