import React from 'react';
import '../styles/Mobile.css';
import {connect} from "react-redux"
import {BrowserRouter as Router } from 'react-router-dom'
import {CSSTransition} from "react-transition-group"

import Nav from "./Nav"
import Routed from "./jsx/routed"
import Login from "./views/Login"
import Processing from "./views/Processing"

import {GetUser} from "../actions/user"

class Mobile extends React.Component {

    componentDidMount = () => {
        document.documentElement.style.setProperty("--app-height", window.innerHeight + "px")
        window.onresize = () => {document.documentElement.style.setProperty("--app-height", window.innerHeight + "px")}
        setInterval(() => {
            this.props.dispatch({type: "IncTimeIn"})
        }, 1000)
        this.props.dispatch(GetUser())
        console.log(process.env)
    }

    render() {
        var currentView
        if (this.props.loggedIn) {
            currentView = <Routed></Routed>
        } else {
            currentView = <Login></Login>
        }
        if (this.props.processing) {
            currentView = <Processing></Processing>
        }
        return (
            <Router>
                <div className="scene" ref="scene">
                    <Nav></Nav>
                    <CSSTransition
                        in = {this.props.navOpened}
                        timeout={{ enter: 300, exit: 300 }}
                        classNames="fade">
                            <div className="curtain" onClick={() => {this.props.dispatch({type: "SetNavOpened", value: false})}}/>
                    </CSSTransition>
                    {currentView}
                </div>
            </Router>
        );
    }
}

const mapStateToProps = (state) => ({
    navOpened: state.nav.opened,
    loggedIn: state.user.loggedIn,
    processing: state.share.processing
});
export default connect(mapStateToProps)(Mobile);
