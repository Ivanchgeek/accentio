import React from 'react';
import { BrowserView, MobileView } from "react-device-detect";
import '../styles/App.sass';
import Desktop from "./Desktop"
import Mobile from "./Mobile"

function App() {
  return (
    <>
      <BrowserView>
        <Desktop></Desktop>
      </BrowserView>
      <MobileView>
        <Mobile></Mobile>
      </MobileView>
    </>
  );
}

export default App;
