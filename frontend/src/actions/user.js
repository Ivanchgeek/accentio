export const GetUser = () => {
    return dispatch => {
        dispatch({type: "SetProcessing", value: true})
        fetch("/user/me", {
            method: "get"
        }).then((resp) => {
            if (resp.status == 401) {
                dispatch({type: "SetUserLoggedIn", value: false})
                dispatch({type: "SetProcessing", value: false})
                throw "auth"
            }
            if (resp.ok) {
                return resp.json()
            }
            throw resp.statusText
        }).then((json) => {
            json.TimeIn = json.TimeIn / 1000000000
            dispatch({type: "SetUser", value: json})
            dispatch({type: "SetUserLoggedIn", value: true})
            dispatch({type: "SetProcessing", value: false})
            var ponger = new WebSocket("wss://localhost:8080/user/attach")
            dispatch({type: "SetPonger", value: ponger})
        }).catch((e) => {
            if (e != "auth") {
                alert("Error: " + e)
            }
        })
    }
};

export const UpdateUser = (timeIn) => {
    return dispatch => {
        fetch("/user/me", {
            method: "get"
        }).then((resp) => {
            if (resp.ok) {
                return resp.json()
            }
            throw resp.statusText
        }).then((json) => {
            json.TimeIn = timeIn
            dispatch({type: "SetUser", value: json})
        }).catch((e) => {
            alert("Error: " + e)
        })
    }
}

export const LogoutUser = () => {
    return dispatch => {
        dispatch({type: "SetProcessing", value: true})
        fetch("/user/logout", {
            method: "get"
        }).then((resp) => {
            if (resp.ok) {
                dispatch({type: "SetUserLoggedIn", value: false})
                dispatch({type: "SetProcessing", value: false})
                dispatch({type: "SetUser", value: {}})
                throw "ok"
            }
            throw resp.statusText
        }).catch((e) => {
            if (e != "ok") {
                alert("Error: " + e)
            }
        })
    }
}