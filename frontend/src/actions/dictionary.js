export const RegisterWordStep = (word, step) => {
    return dispatch => {
        fetch("/dictionary/register?word="+word+"&step="+step, {
            method: "get"
        }).then((resp) => {
            if (!resp.ok) {
                console.log(resp.statusText)
            }
        })
    }
};

export const InitWordDistribution = (dict) => {
    return dispatch => {
        var distribution = []
        for (var i = 0; i < dict.length; i++) {
            if (dict[i].Count >= 10) {
                var distr = Math.ceil((100 - dict[i].Percentage + 10) / 5)
                console.log(dict[i].Content + " distr: " + distr)
                for (var j = 0; j <= distr; j++) {
                    distribution.push(dict[i])
                }
            } else {
                console.log(dict[i].Content + " distr: 22 (less info)")
                for (var j = 0; j < 22; j++) {
                    distribution.push(dict[i])
                }
            }
        }
        dispatch({type: "SetWordDistribution", value: distribution})
    }
}